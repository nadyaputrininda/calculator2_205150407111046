package easy.tuto.easycalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {
    private TextView hasil, solusi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        hasil = findViewById(R.id.hasil);
        solusi = findViewById(R.id.solusi);
        String finalResult = getIntent().getStringExtra("finalResult");
        String solution = getIntent().getStringExtra("solution");

        hasil.setText(finalResult);
        solusi.setText(solution);
    }
}